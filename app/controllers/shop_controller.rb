class ShopController < ApplicationController
  before_action :set_cart

  def index
    @products = Product.all
  end

  private
    def set_cart
      @cart = Cart.where(:active => true, :id => session[:cart_id]).first
      return if !@cart.nil?
      @cart = Cart.create!(:active => true)
      session[:cart_id] = @cart.id
    end

end
