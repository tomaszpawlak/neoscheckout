class Cart::CheckOutController < ShopController

  def index
    @products = @cart.products
  end

  def check_out
    @customer = Customer.new(:names => params[:names],:email => params[:email], :card_type => params[:card_type], :card_number => params[:card_number], :card_cv_code => params[:card_cv_code], :card_expiration_date => params[:card_expiration_date])
    if @customer.save
     @order = Order.create!(:customer => @customer, :cart => @cart)
     @cart.deactivate
     redirect_to order_confirmation_path(@order)
    else
      render :index
    end
  end

end
