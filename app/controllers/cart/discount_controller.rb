class Cart::DiscountController < ShopController

  def add_discount
    discount = Discount.where(:code => params[:discount_code]).first
    if !discount.nil? && (!@cart.discounts.any? || @cart.allow_discount_conjuction?(discount.conjuction)) && !@cart.discounts.include?(discount)
      @cart.discounts << discount
    end
    redirect_to root_path
  end

  def remove_discount
    @cart.discounts.delete(Discount.find(params[:id]))
    redirect_to root_path
  end

end
