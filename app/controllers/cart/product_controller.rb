class Cart::ProductController < ShopController

  def add_product
    current_product = @cart.cart_products.where(:product_id => params[:id]).first
    if current_product.nil?
      CartProduct.create(:cart => @cart, :product => Product.find(params[:id]), :count => params[:count].empty? ? 1 : params[:count] )
    else
      CartProduct.where(:cart => @cart, :product => current_product.product_id).update_all("count = count + #{params[:count]}")
    end
    redirect_to root_path
  end

  def remove_product
    current_product, count = @cart.cart_products.where(:product_id => params[:id]).first, params[:count]
    if params[:count].nil? || (current_product.count-params[:count].to_i) < 1
      @cart.cart_products.delete(current_product)
    else
      current_product.update(:count => current_product.count-params[:count].to_i)
    end
    redirect_to root_path
  end

end
