class OrderController < ShopController

  def confirmation
    @order = Order.find(params[:id])
  end

  def track
    @order = Order.where(:reference => params[:reference]).first if !params[:reference].nil?
  end

end
