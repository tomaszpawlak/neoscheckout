class Customer < ApplicationRecord
  has_many :orders

  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates :names, length: { minimum: 3 }
  validates :card_cv_code, length: {is: 4}
  validates :card_type, presence: true
  validates :card_number, length: {in: 7..10}

end
