class Cart < ApplicationRecord

  belongs_to :order, optional: true

  has_many :cart_discounts
  has_many :discounts, through: :cart_discounts

  has_many :cart_products
  has_many :products, through: :cart_products

  def products
    products = []
    self.cart_products.each do |association|
      product= Product.find(association.product_id)
      product.count, product.sum_price = association.count, (product.price*association.count) #virtual attributes
      products.push(product)
    end unless !self.cart_products.any?
    return products
  end

  def total_products_price
    sum = 0
    self.products.each { |a| sum+=a.sum_price}
    return sum
  end

  def total_price
    total_price = self.total_products_price
    self.discounts.order("reduction_percentage").each { |discount| total_price-=discount.reduce_amount(total_price)}
    return total_price.round(2)
  end

  def allow_discount_conjuction?(conjuction)
    return true if !self.discounts.any?
    return !conjuction ? false : self.discounts.first.conjuction
  end

  def deactivate
    self.update!(:active => false)
  end

end
