class Discount < ApplicationRecord

  def reduce_amount total_price
    return self.reduction_value if !self.reduction_percentage
    return total_price*(0.01*self.reduction_value)
  end

end
