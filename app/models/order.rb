class Order < ApplicationRecord

  belongs_to :customer
  has_one :cart
  after_create :generate_key

  def generate_key
    reference = rand(2**256).to_s(36)[0..7]
    while !Order.where(:reference => reference).first.nil? do
      reference = rand(2**256).to_s(36)[0..7]
    end
    self.update(:reference => reference )
    self.save
  end

end
