Rails.application.routes.draw do

  root 'shop#index'
  get 'shop/index'

  namespace :cart do
    get 'add_product/:id', controller: :product, action: :add_product, :as => 'add_product'
    get 'remove_product/:id', controller: :product, action: :remove_product, :as => 'remove_product'
    post 'add_discount', controller: :discount, action: :add_discount, :as => 'add_discount'
    get 'remove_discount/:id', controller: :discount, action: :remove_discount, :as => 'remove_discount'
    get 'check-out', controller: :check_out, action: :index, :as => 'check_out'
    get 'check-out-processing', controller: :check_out, action: :check_out, :as => 'check_out_processing'
  end

  post 'order/create', controller: :order, action: :order_creation, :as => 'order_creation'
  get  'order-confirmation/:id', controller: :order, action: :confirmation, :as => 'order_confirmation'
  get  'order/track', controller: :order, action: :track, :as => 'order_tracking' 

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
