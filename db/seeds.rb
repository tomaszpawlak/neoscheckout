# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Product.create([
  { title: 'Smart Hub', price: 49.99  },
  { title: 'Motion Sensor', price: 29.99  },
  { title: 'Wireless Camera', price: 99.99  },
  { title: 'Smoke Sensor', price: 19.99  },
  { title: 'Water Leak Sensor', price: 14.99  },
])

Discount.create([
  { name: "20% off final cost", code: "20%OFF", reduction_value: 20.00, reduction_percentage: true, conjuction: false },
  { name: "5% off final cost", code: "5%OFF", reduction_value: 5.00, reduction_percentage: true, conjuction: true },
  { name: "£20 off final cost", code: "20POUNDSOFF", reduction_value: 20.00, reduction_percentage: false, conjuction: true }
])
