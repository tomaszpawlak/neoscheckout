class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :names
      t.string :email
      t.string :card_type
      t.integer :card_number
      t.integer :card_cv_code, limit: 2
      t.date :card_expiration_date

      t.timestamps
    end
  end
end
