class AddReferences < ActiveRecord::Migration[5.1]
  def change

    add_reference :carts, :order, index: true, foreign_key: true
    add_reference :orders, :customer, index: true, foreign_key: true

  end
end
