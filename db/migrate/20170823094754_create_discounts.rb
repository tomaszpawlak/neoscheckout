class CreateDiscounts < ActiveRecord::Migration[5.1]
  def change
    create_table :discounts do |t|
      t.string :name
      t.string :code
      t.decimal :reduction_value
      t.boolean :reduction_percentage
      t.boolean :conjuction

      t.timestamps
    end
  end
end
