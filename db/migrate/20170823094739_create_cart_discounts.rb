class CreateCartDiscounts < ActiveRecord::Migration[5.1]
  def change
    create_table :cart_discounts do |t|
      t.integer :cart_id
      t.integer :discount_id

      t.timestamps
    end
  end
end
